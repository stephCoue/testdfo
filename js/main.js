(function(){

  var $searchDetail = $('.search-detail');

  $('#miniSearch').on('click', "input", function(){
     $searchDetail.show();
  });

  $searchDetail.on('click', ".icon-btn-close", function(evt){
    evt.preventDefault();
    $searchDetail.hide();
  });

  $searchDetail.on('click', '#collapse-services-promo', function(evt){
    evt.preventDefault();
    $('.services-checkbox').toggle();
    $('.promo-code').toggle();
  });

})();