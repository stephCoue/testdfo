# Test technique intégration web, validation du niveau du candidat


##Critères de notation :  - Sémantique HTML, validité du code et SEO - Intégration flexible, fluide - Qualité du rendu sous l’ensemble des navigateurs et animation de l’interface - Qualité des développements Javascript et conformité des comportements - Choix techniques : Vous expliquerez en quelques lignes vos choix techniques
 

## Mes Choix:
J'aurai pu partir avec un [Foundation](http://foundation.zurb.com/) ou [boostraps](http://getbootstrap.com/) mais cela n'a pas été le cas from scratch comme les vrais…

Aidé de [Compass](http://compass-style.org/) + [susy](http://susy.oddbird.net/) pour styler mes css, [Grunt](http://gruntjs.com) pour 2-3 taches d'aide au dev et m'éviter de faire des cmd+r à répétition.
Mon but était de supporter **ie8 min** mais après quelques jours de dev et à J+3 1er test sous ie  arff…
  
Au final testé sur:

 - Canary       ( MacOs )
 - chrome 31    ( MacOS linux Windows7)
 - Firefox 27   ( MacOs & Windows7)
 - firefox 12   ( Window7)
 - firefox 25   ( linux )
 - Safari  		( MacOs & Windows7)
 - ie10 & 9
 - ie8 & 7
 
 
 
>  "Parcequ'un peu de code vaut mieux qu'un long discours"
 
`$ git clone git@bitbucket.org/stephCoue/testdfo.git`

`$ npm install`

`$ grunt`


 



